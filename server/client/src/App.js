import React from "react";
import { Alert, Button, Container, Form, FormControl, InputGroup, Jumbotron, Table } from "react-bootstrap";
import calcUtil from "./utils/calc.js";

import 'bootstrap/dist/css/bootstrap.min.css';

function request(url, options = { method: "GET" }) {
  return fetch(url, options)
    .then((res) => res.json())
    .catch(() => ({
      type: "error",
      message: "Couldn't find server",
    }))
    .then((result) => {
      if (result.type === "success") {
        return result;
      }
      throw new Error(result.message);
    });
}

class AppComponent extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      username: "",
      cache: [],
      cmd: "",
      res: "",
      lastOperator: 0,
      errorMsg: "",
    };

    this.insertOperator = this.insertOperator.bind(this);
    this.insertOperand = this.insertOperand.bind(this);
    this.clear = this.clear.bind(this);
    this.handleCalculate = this.handleCalculate.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  insertOperator(val) {
    this.setState({
      cmd: this.state.cmd ? this.state.cmd + " " + val : val,
      lastOperator: 0,
      errorMsg: "",
    })
  }

  insertOperand(val) {
    this.setState({
      cmd: this.state.lastOperator === 0 ? this.state.cmd + " " + val : this.state.cmd + val,
      lastOperator: this.state.lastOperator + 1,
      errorMsg: ""
    })
  }

  clear(all = false) {
    this.setState({
      cmd: all ? "" : this.state.cmd.slice(0, -1).trim(),
      lastOperator: all ? 0 : Math.max(0, this.state.lastOperator - 1),
      errorMsg: "",
      res: all ? "" : this.state.res
    })
  }

  handleCalculate() {
    let res = "", errorMsg = "";
    try {
      res = calcUtil(this.state.cmd).toString();
    } catch(err) {
      errorMsg = err.message;
    }
    this.setState({
      res, errorMsg
    })
  }

  handleSubmit() {
    let res = "", errorMsg = "";
    try {
      res = calcUtil(this.state.cmd).toString();
    } catch(err) {
      errorMsg = err.message;
    }
    if(errorMsg) {
      this.setState({
        errorMsg
      });
      return;
    }
    if(this.state.username.trim() === "" || this.state.cmd.trim() === "") {
      this.setState({
        errorMsg: "Set a username and cmd!"
      })
      return;
    }
    return request(`/insert/${
      encodeURIComponent(this.state.username.trim())
    }/${
      encodeURIComponent(this.state.cmd.trim())
    }/${
      encodeURIComponent(res.trim())
    }`, {
      method: "POST",
    })
    .then(ret => request("/cache"))
    .then(ret =>
      this.setState({
        res,
        cache: JSON.parse(ret.message) || []
      })
    )
  }

  componentDidMount() {
    return request("/cache")
    .then(ret =>
      this.setState({
        cache: JSON.parse(ret.message) || []
      })
    )
  }



  render() {
    return (
      <Container>
        <Jumbotron style={{margin: "2em"}}>
        <h1>Calculator</h1>
        <p>by Rajan Dalal</p>
        </Jumbotron>

        <Form>
          <Form.Group controlId="username">
            <Form.Label>Username</Form.Label>
            <Form.Control
              type="text"
              onChange={(e) => this.setState({username: e.target.value, errorMsg: ""})}
              value={this.state.username}
            />
          </Form.Group>
        </Form>

        <br />

        <InputGroup className="mb-3">
          <InputGroup.Prepend>
            <InputGroup.Text id="eval">cmd</InputGroup.Text>
          </InputGroup.Prepend>
          <FormControl
            readonly
            value={this.state.cmd}
          />
        </InputGroup>

        <InputGroup className="mb-3">
          <InputGroup.Prepend>
            <InputGroup.Text id="res">val</InputGroup.Text>
          </InputGroup.Prepend>
          <FormControl
            readonly
            value={this.state.res}
          />
        </InputGroup>


        <Container fluid>
        <Table borderless size="sm">
          <tbody>
            <tr>
              <td><Button variant="outline-danger" onClick={() => this.clear(true)}>AC</Button></td>
              <td><Button variant="outline-primary" onClick={() => this.insertOperator("(")}>(</Button></td>
              <td><Button variant="outline-primary" onClick={() => this.insertOperator(")")}>)</Button></td>
              <td><Button variant="outline-danger" onClick={() => this.clear()}>X</Button></td>
            </tr>
            <tr>
              <td><Button variant="outline-primary"  onClick={() => this.insertOperand("7")}>7</Button></td>
              <td><Button variant="outline-primary"  onClick={() => this.insertOperand("8")}>8</Button></td>
              <td><Button variant="outline-primary"  onClick={() => this.insertOperand("9")}>9</Button></td>
              <td><Button variant="outline-secondary" onClick={() => this.insertOperator("/")}>{"/"}</Button></td>
            </tr>
            <tr>
              <td><Button variant="outline-primary"  onClick={() => this.insertOperand("4")}>4</Button></td>
              <td><Button variant="outline-primary"  onClick={() => this.insertOperand("5")}>5</Button></td>
              <td><Button variant="outline-primary"  onClick={() => this.insertOperand("6")}>6</Button></td>
              <td><Button variant="outline-secondary" onClick={() => this.insertOperator("*")}>{"x"}</Button></td>
            </tr>
            <tr>
              <td><Button variant="outline-primary"  onClick={() => this.insertOperand("1")}>1</Button></td>
              <td><Button variant="outline-primary"  onClick={() => this.insertOperand("2")}>2</Button></td>
              <td><Button variant="outline-primary"  onClick={() => this.insertOperand("3")}>3</Button></td>
              <td><Button variant="outline-secondary" onClick={() => this.insertOperator("+")}>{"+"}</Button></td>
            </tr>
            <tr>
            <td><Button variant="outline-primary"  onClick={() => this.insertOperand("0")}>0</Button></td>
            <td><Button variant="outline-primary"  onClick={() => this.insertOperand(".")}>.</Button></td>
            <td><Button variant="outline-secondary" onClick={() => this.insertOperator("^")}>{"^"}</Button></td>
            <td><Button variant="outline-secondary" onClick={() => this.insertOperator("-")}>{"-"}</Button></td>
            </tr>
            <tr>
            <td></td>
            <td></td>
            <td><Button variant="primary"  onClick={() => this.handleCalculate()}>Calculate</Button></td>
            <td><Button variant="primary"  onClick={() => this.handleSubmit()}>Submit</Button></td>
            </tr>
          </tbody>
        </Table>
        </Container>
        {this.state.errorMsg && <Alert variant="danger">{this.state.errorMsg}</Alert>}
        <Container>
        <Table striped bordered hover>
            <thead>
              <tr>
                <th>Username</th>
                <th>Command</th>
                <th>Result</th>
                {/*<th>Timestamp</th>*/}
              </tr>
            </thead>
            <tbody>
            {this.state.cache.map(e => (
              <tr>
                <td>{decodeURIComponent(e.username)}</td>
                <td>{decodeURIComponent(e.cmd)}</td>
                <td>{decodeURIComponent(e.res)}</td>
                {/*<td>{e.latest}</td>*/}
              </tr>
            ))}
            </tbody>
          </Table>
        </Container>
      </Container>
    )
  }
}

function App() {
 return (
   <AppComponent />
 )
}

export default App;
