const add = (a, b) => a + b;
const subtract = (a, b) => a - b;
const div = (a, b) => {
  if (b === 0) throw new Error("Division by zero!");
  return  a / b;
}
const mod = (a, b) => {
  if (b === 0) throw new Error("Remainder by zero!");
  return  a % b;
}
const mulp = (a, b) => a * b;
const power = (a, b) => a ** b;

const OPERATORS = {"+": add, "-": subtract, "*": mulp, "/": div, "%": mod, '^': power};
const OPERATORS_LIST = Object.keys(OPERATORS);
const PRECEDENCE = {"^": 3, "*": 2, "/": 2, "%": 2, "+": 1, "-": 1};
const OPEN = "(";
const CLOSE = ")";

/**
 * Checks if valid digit. Null if not.
 */
function _getDigit(str) {
  if (str == null) return null;

  if(OPERATORS_LIST.includes(str) || str === OPEN || str === CLOSE) {
    return null;
  }

  try {
    const ret = Number(str);
    if(isNaN(ret)) return null;
    return ret;
  } catch(err) {
    return null;
  }
}

/**
 * Peeks stack. undefined if empty.
 */
function _peek(stack) {
  if(stack.length) return stack.slice(-1)[0];
  else return undefined;
}

/**
 * Infix 2 postfix
 */
function _infixToPostfix(cmd) {
  let stack = [OPEN];
  let postfix = [];

  const tokens = cmd.concat(` ${CLOSE}`).split(" ").filter(x => x !== "");

  for (let i = 0; i < tokens.length; i++) {
    const curr_token = tokens[i];

    if(curr_token === OPEN) {
      stack.push(curr_token);
    }
    else if(curr_token === CLOSE) {
      let stack_op = _peek(stack);
      while(stack_op != null && stack_op !== OPEN) {
        postfix.push(stack_op);
        stack.pop();
        stack_op = _peek(stack);
      }
      if(stack_op === OPEN) {
        stack.pop();
      }
    }
    else if(OPERATORS_LIST.includes(tokens[i])) {
      // unary check
      if(i === 0 || tokens[i - 1] === OPEN || OPERATORS_LIST.includes(tokens[i - 1])) {
        if (_getDigit(tokens[i + 1]) == null) {
          throw new Error(`Token ${curr_token} at ${i} left bereft`);
        }
        postfix.push(0);
        postfix.push(tokens[++i]);
        postfix.push(curr_token);
      }
      // is binary
      else {
        let stack_op = _peek(stack);
        while(OPERATORS_LIST.includes(stack_op) && PRECEDENCE[stack_op] >= PRECEDENCE[curr_token]) {
          postfix.push(stack_op);
          stack.pop();
          stack_op = _peek(stack);
        }
        stack.push(curr_token);
      }
    }
    else {
      const digit = _getDigit(curr_token);
      if(digit == null) {
        throw new Error(`Unknown number ${curr_token} at ${i}`)
      }
      postfix.push(digit)
    }
  }
  if (stack.length !== 0) {
    throw new Error(`Wrong expression. ${stack.length} unlinked tokens`)
  }
  return postfix;
}

/**
 * Takes a command string and computes via stack. Errors if bad input.
 */
function calcUtil(cmd) {
  const postfix = _infixToPostfix(cmd);
  let stack = [];
  for (const op of postfix) {
    if (OPERATORS_LIST.includes(op)) {
      const right_op = stack.pop();
      const left_op = stack.pop();
      stack.push(OPERATORS[op](left_op, right_op))
    } else {
      stack.push(op)
    }
  }
  if(stack.length !== 1 || _getDigit(stack[0]) == null) {
    throw new Error(`Something's wrong friend: ${stack}`);
  }
  return stack[0];
}

export default calcUtil;
