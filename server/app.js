const express = require('express');
const path = require('path');

const app = express();
app.use(express.static(path.join(__dirname, './client/build')));

const CACHE_LEN = 10;
let cache = [];
let cache_lock = false;
let latest = Date.now();

function _updateCache(val) {
  if(cache_lock) return process.nextTick(_updateCache, val);
  cache_lock = true;
  cache.push(val);
  cache = cache.slice(-CACHE_LEN);
  latest = Date.now();
  cache_lock = false;

  return { latest, cache };
}

app.get("/latest", (req, res) => {
  res.status(200).json(
    {
      type: "success",
      message: latest,
    }
  );
});

app.get("/cache", async (req, res) => {
  res.status(200).json(
    {
      type: "success",
      message: JSON.stringify(cache),
    }
  );
});

app.post("/insert/:username/:cmd/:res", async (req, res) => {
  const validate = (params) =>
    params &&
    params.username !== undefined &&
    params.cmd !== undefined &&
    params.res !== undefined;

  if (!validate(req.params)) {
    res.status(400).json(
      {
         type: "error",
         message: "Incorrect body",
     }
    );
  }

  res.status(200).json(
    {
      type: "success",
      message: JSON.stringify(_updateCache(req.params)),
    }
  );
});

app.get("*", (req, res) => {
  res.sendFile(path.resolve(__dirname,  "./client/build", "index.html"));
});

app.listen(process.env.PORT || 5000);
